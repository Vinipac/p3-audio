#include "tratamentoAudio.h" //to do
#include "readInput.h" //to do

void mudaConjunto(short* *vet, int tam, int i){
  int cont;
  int aux;

  aux = *vet[i];
  *vet[i] = *vet[tam-i-1]; // -1 necessario ja que vetor começa em 0
  *vet[tam-i-1] = *vet[i];
}


int main(int argc, char const *argv[]){
  short* *vet_amostra, *saida_header;
  struct wav_T audio_header;
  FILE * arq_final;
  int i;
  char input, output;

  for(i=1; i<argc; i++){
    switch (*argv[i]) {
      case "-i":{
        input = (char)malloc(sizeof(*argv[i+1]));
        input = *argv[i+1];
      }
      case "-o":{
        input = (char)malloc(sizeof(*argv[i+1]));
        output = *argv[i+1];
      }
    }
  }

  readInput(input, &vet_amostra, &saida_header);
  fread(&audio_header, sizeof(struct wav_T), 1, saida_header);
  int numChan = audio_header.Number_of_channels;
  int tam_Amostra = audio_header.SubChunk2Size;
  for (i=0;i < (tam_Amostra/2 /* metade dos conjuntos de amostras */); i += numChan){
    mudaConjunto(&vet_amostra, tam_Amostra, i);
  }

  fopen(output, "w");
  //conferir abaixo dps
  arq_final=malloc(tam_Amostra * sizeof(short) + sizeof(audio_header));
  fwrite(arq_final, sizeof(struct wav_T), 1, saida_header);
  fseek(arq_final, sizeof(audio_header), SEEK_SET);
  fwrite(arq_final, sizeof(short), audio_header.SubChunk2Size, vet_amostra);

  //coloca arq_final em exemplo


  return 0;
}
