#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

struct wav_T
{
	char     ChunkID[4];
	uint32_t ChunkSize;
	char     Format[4];
	char     SubChunk1ID[4];
	uint32_t SubChunk1Size;
	uint16_t Audio_Format;
	uint16_t Number_of_channels;
	uint32_t Sample_Rate;
	uint32_t Byte_rate;
	uint16_t Block_align;
	uint16_t Bits_per_sample;
	char     SubChunk2ID[4];
	uint32_t SubChunk2Size;
};
