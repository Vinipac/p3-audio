//falta sesolver warnings
//segmentation fault

#include "readInput.h"

int main(int argc, char const *argv[])
{
	FILE *vet_amostra_A, *saida_header_A;
 	struct wav_T audio_header_A;

 	FILE *vet_amostra_B, *saida_header_B;
 	struct wav_T audio_header_B;

 	FILE * arq_final, *saida_header_final;
 	long int tam_Amostra_Final;

	int i;
	char input, output;

	for(i=0; i<argc; i++){
		if ((*argv[i]) == "-i"){
			input = malloc(sizeof(*argv[i+1]));
			input = *argv[i+1];
		}
		if ((*argv[i]) == "-o"){
			output = malloc(sizeof(*argv[i+1]));
			output = *argv[i+1];
		}
	}

	readInput(input, &vet_amostra_A, &saida_header_A);
  fread(&audio_header_A, sizeof(struct wav_T), 1, saida_header_A);
  long int tam_Amostra_A = audio_header_A.SubChunk2Size;

  readInput(input, &vet_amostra_B, &saida_header_B);
  fread(&audio_header_B, sizeof(struct wav_T), 1, saida_header_B);
  long int tam_Amostra_B = audio_header_B.SubChunk2Size;

  tam_Amostra_Final = tam_Amostra_A + tam_Amostra_B;
  struct wav_T audio_header_Final;
  audio_header_Final = audio_header_A;
  audio_header_Final.SubChunk2Size = tam_Amostra_A + tam_Amostra_B;

 	fwrite(&audio_header_Final, sizeof(struct wav_T), 1, saida_header_final); // Erro no 4o argumento

  arq_final = fopen(output, "w");
  arq_final = malloc(tam_Amostra_Final * sizeof(short) + sizeof(struct wav_T));
  fwrite(arq_final, sizeof(struct wav_T), 1, saida_header_final);
  fseek(arq_final, sizeof(struct wav_T), SEEK_SET);

  fwrite(arq_final, sizeof(short), audio_header_A.SubChunk2Size, vet_amostra_A);
  fseek(arq_final, sizeof(vet_amostra_A), SEEK_SET);

  fwrite(arq_final, sizeof(short), audio_header_B.SubChunk2Size, vet_amostra_B);

  // escreve a saida propriamente dita


	return 0;
}

/* Algoritmo

vet_A;
vet_B;
vet_C

tam_A;
tam_B;
tam_C;

int i;

for(i=0; i<tam_A; i++){
  vet_C[i] = vet_A[i];
}
for(i=i; i<tam_B; i++){
  vet_C[i] = vet_B[i];
}

tam_C = tamA + tam_B;
*/
